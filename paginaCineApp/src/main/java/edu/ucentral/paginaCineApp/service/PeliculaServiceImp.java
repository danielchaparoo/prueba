package edu.ucentral.paginaCineApp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import edu.ucentral.paginaCineApp.model.Horario;
import edu.ucentral.paginaCineApp.model.Pelicula;
import edu.ucentral.paginaCineApp.model.SalaCine;

@Service
public class PeliculaServiceImp implements PeliculaService{

	private List<Pelicula> lista;
	
	public PeliculaServiceImp() {
		lista=new ArrayList<>();
		Pelicula pelicula=new Pelicula();
		HorarioServiceImp horario= new HorarioServiceImp();
		SalaCineServiceImp salaCine= new SalaCineServiceImp();
		
		pelicula.setId(1);
		pelicula.setNombre("Rapidos y Furiosos");
		pelicula.setClasificacion("+18");
		pelicula.setGenero("Accion");
		pelicula.setImage("logo1.png");
		pelicula.setCartelera(true);
		pelicula.setDuracion(120);
		pelicula.setSala(salaCine.buscarSalaCine(1));
		pelicula.setDescripcion("Dominic Torretto emprende un viaje...");
		pelicula.setDirector("Justin Lin");
		pelicula.setProductora("Universal Pictures");
		pelicula.setActores("Vin Diesel, Neal H. Moritz, Michael Fottrell, Michael K. Ross");
		pelicula.setHorario(horario.buscarHorario(1));
		
		lista.add(pelicula);
		
		pelicula= new Pelicula();
		pelicula.setId(2);
		pelicula.setNombre("Tenet");
		pelicula.setClasificacion("+16");
		pelicula.setGenero("Accion,Cinecia Ficcion");
		pelicula.setImage("logo2.png");
		pelicula.setCartelera(true);
		pelicula.setDuracion(120);
		pelicula.setSala(salaCine.buscarSalaCine(2));
		pelicula.setDescripcion("Un agente secreto emprende una misión que se desarrolla más allá del tiempo real, para intentar prevenir una Tercera Guerra Mundial.");
		pelicula.setDirector("Christopher Nolan");
		pelicula.setProductora("Warner Bros. Pictures\r\n"
				+ "Syncopy Films");
		pelicula.setActores("John David Washington\r\n"
				+ "Robert Pattinson\r\n"
				+ "Elizabeth Debicki\r\n"
				+ "Dimple Kapadia\r\n"
				+ "Michael Caine\r\n"
				+ "Kenneth Branagh");
		pelicula.setHorario(horario.buscarHorario(2));
		
	}
	
	
	@Override
	public List<Pelicula> listarPeliculas() {
		
		return lista;
	}

	@Override
	public void guardarPelicula(Pelicula pelicula) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Pelicula buscarPelicula(int id) {
		for (Pelicula pelicula : lista) {
			if(pelicula.getId()==id) {
				return pelicula;
			}
		}
		
		return null;
	}

	@Override
	public void modificarPelicula(Pelicula pelicula) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarPelicula(int id) {
		// TODO Auto-generated method stub
		
	}

	
}
