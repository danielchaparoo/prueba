package edu.ucentral.paginaCineApp.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import edu.ucentral.paginaCineApp.model.Horario;
import edu.ucentral.paginaCineApp.service.HorarioServiceImp;

public class main {
	
	public static void main(String[] args) {
		
	
	HorarioServiceImp horario = new HorarioServiceImp();
	 SimpleDateFormat sd= new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
	List<Horario> f=  horario.listarHorarios();
	for(Horario h: f) {
	      System.out.println(sd.format( h.getHorario()));	
	  }
	}
}

