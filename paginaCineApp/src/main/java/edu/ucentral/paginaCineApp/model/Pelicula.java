package edu.ucentral.paginaCineApp.model;

import java.awt.Image;
import java.util.ArrayList;

public class Pelicula {
	private int id;
	private String nombre;
	private String clasificacion;
	private String genero;
	private String image;
	private boolean cartelera;
	private int duracion;
	private SalaCine sala;
	private String descripcion;
	private String director;
	private String productora;
	private String actores;
	private Horario horario;
	
	public Pelicula() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClasificacion() {
		return clasificacion;
	}
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public boolean isCartelera() {
		return cartelera;
	}
	public void setCartelera(boolean cartelera) {
		this.cartelera = cartelera;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public SalaCine getSala() {
		return sala;
	}
	public void setSala(SalaCine sala) {
		this.sala = sala;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getProductora() {
		return productora;
	}
	public void setProductora(String productora) {
		this.productora = productora;
	}
	public String getActores() {
		return actores;
	}
	public void setActores(String actores) {
		this.actores = actores;
	}
	public Horario getHorario() {
		return horario;
	}
	public void setHorario(Horario horario) {
		this.horario = horario;
	}
	
	
	
}
