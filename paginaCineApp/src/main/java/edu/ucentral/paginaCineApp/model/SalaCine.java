package edu.ucentral.paginaCineApp.model;

import java.util.ArrayList;

public class SalaCine {
	private int id;
	private int numeroSala;
	private int cantidadAsientos;
	
	public SalaCine(){
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumeroSala() {
		return numeroSala;
	}
	public void setNumeroSala(int numeroSala) {
		this.numeroSala = numeroSala;
	}
	public int getCantidadAsientos() {
		return cantidadAsientos;
	}
	public void setCantidadAsientos(int cantidadAsientos) {
		this.cantidadAsientos = cantidadAsientos;
	}


}
