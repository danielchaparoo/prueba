package edu.ucentral.paginaCineApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.ucentral.paginaCineApp.model.Boleta;
import edu.ucentral.paginaCineApp.model.Pelicula;
import edu.ucentral.paginaCineApp.model.SalaCine;
import edu.ucentral.paginaCineApp.model.Usuario;
import edu.ucentral.paginaCineApp.service.BoletaService;
import edu.ucentral.paginaCineApp.service.PeliculaService;
import edu.ucentral.paginaCineApp.service.SalaCineService;
import edu.ucentral.paginaCineApp.service.UsuarioService;

@Controller
@RequestMapping(value="/boletas")
public class BoletaController {
	
	@Autowired
	private BoletaService service;
	
	@Autowired
	private SalaCineService service1;
	
	@Autowired
	private PeliculaService service2;
	
	@Autowired
	private UsuarioService service3;
	
	@GetMapping("/index")
	public String mostrarIndex(Model model) {
		List<Boleta> lista= service.listarBoletas();
		model.addAttribute("boletas", lista);
		return "boletas/index";
	} 
	
	@GetMapping("/crear")
	public String crearBoleta(Model model) {
		
		List<SalaCine> lista=service1.listarSalasCine();
		List<Pelicula> lista2= service2.listarPeliculas();
		List<Usuario> lista3= service3.listarUsuarios();
				
		model.addAttribute("salasCine" ,lista);
		model.addAttribute("horarios",lista2);
		model.addAttribute("usuarios",lista3);
		return "boletas/formBoleta";
	}
	
	@GetMapping("/ver/{id}")
	public String buscarBoleta(@PathVariable int id,Model model) {
		Boleta boleta=service.buscarBoleta(id);
		model.addAttribute("boletas",boleta);
		return "boletas/formModificar";
	}
	
	@PostMapping("/modicar")
	public String modificarBoleta(Boleta boleta) {
		service.modificarBoleta(boleta);
		return "redirect:/boletas/index";
	}

	@GetMapping("/eliminar/{id}")
	public String eliminarBoleta(@PathVariable int id) {
		service.eliminarBoleta(id);
		return "redirect:/boletas/index";
	}
	
	@PostMapping("/guardar")
	public String guardarBoleta(Boleta boleta) {
		service.guardarBoleta(boleta);
		return "redirect:/boletas/index";
	}
	
	
}
