package edu.ucentral.paginaCineApp.model;

import java.time.LocalDate;
import java.util.Date;

public class Horario {
	
	private int id;
	private Date horario;
	
	
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	
	public Horario() {
		
	}

	public Date getHorario() {
		return horario;
	}

	public void setHorario(Date horario) {
		this.horario = horario;
	}
	
	
}
