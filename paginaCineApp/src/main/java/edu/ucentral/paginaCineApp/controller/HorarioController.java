package edu.ucentral.paginaCineApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.ucentral.paginaCineApp.model.Horario;
import edu.ucentral.paginaCineApp.service.HorarioService;

@Controller
@RequestMapping(value="/usuarios")
public class HorarioController {
	@Autowired
	private HorarioService service;
	
	@GetMapping("/index")
	public String mostrarIndex(Model model) {
		List<Horario> lista=service.listarHorarios();
		model.addAttribute("horarios",lista);
		return "usuarios/index";
	}
	
	@GetMapping("/crear")
	public String crearHorario() {
		return "usuarios/formUsuario";
	}
	
	@GetMapping("/ver/{id}")
	public String buscarHorario(@PathVariable int id,Model model) {
		Horario horario=service.buscarHorario(id);
		model.addAttribute("horario",horario);
		return "horarios/formModificar";
	}
	
	
}
