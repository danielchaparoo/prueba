package edu.ucentral.paginaCineApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String inicio(Model model) {
		
		String mensaje="Bienvenido";
		model.addAttribute("mensaje",mensaje);
		return "home";
	}
	
}
