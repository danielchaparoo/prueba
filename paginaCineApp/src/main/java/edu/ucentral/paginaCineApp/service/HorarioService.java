package edu.ucentral.paginaCineApp.service;

import java.util.List;

import edu.ucentral.paginaCineApp.model.Horario;

public interface HorarioService {

	public List<Horario> listarHorarios();
	public void guardarHorario(Horario horario);
	public Horario buscarHorario(int id);
	public void modificarHorario(Horario horario);
	public void eliminarHorario(int id);
	
	
}
