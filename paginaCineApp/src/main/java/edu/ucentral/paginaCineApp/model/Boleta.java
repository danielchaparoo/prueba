package edu.ucentral.paginaCineApp.model;

public class Boleta {
	
	private int id;
	private String codigo;
	private Pelicula pelicula;
	private SalaCine sala;
	private double valor;
	private int asientosApartados;
	private Usuario usuario;
	
	public Boleta() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Pelicula getPelicula() {
		return pelicula;
	}
	public void setPelicula(Pelicula pelicula) {
		this.pelicula = pelicula;
	}
	public SalaCine getSala() {
		return sala;
	}
	public void setSala(SalaCine sala) {
		this.sala = sala;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public int getAsientosApartados() {
		return asientosApartados;
	}
	public void setAsientosApartados(int asientosApartados) {
		this.asientosApartados = asientosApartados;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	
}
