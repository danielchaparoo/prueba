package edu.ucentral.paginaCineApp.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import edu.ucentral.paginaCineApp.model.Horario;

@Service
public class HorarioServiceImp implements HorarioService {
	
	private List<Horario> lista;
	private SimpleDateFormat sd= new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
	
	public HorarioServiceImp()  {
		lista= new ArrayList<>();
		Horario horario=new Horario();
		
		horario.setId(1);
		try {
			horario.setHorario((sd.parse("2021-11-11 14:30:00")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lista.add(horario);
		
		horario=new Horario();
		
		horario.setId(2);
		try {
			horario.setHorario(sd.parse("2021-10-10 09:00:00"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		lista.add(horario);
		
	}
	
	@Override
	public List<Horario> listarHorarios() {
		
		return lista;
	}

	@Override
	public void guardarHorario(Horario horario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Horario buscarHorario(int id) {
		for (Horario horario : listarHorarios()) {
			if(horario.getId()==id) {
			return horario;
			}
		}
		return null;
	}

	@Override
	public void modificarHorario(Horario horario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarHorario(int id) {
		// TODO Auto-generated method stub
		
	}

	
}
