package edu.ucentral.paginaCineApp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import edu.ucentral.paginaCineApp.model.Boleta;
import edu.ucentral.paginaCineApp.service.UsuarioServiceImp;

@Service
public class BoletaServiceImp implements BoletaService {

	private List<Boleta> lista;
	
	public BoletaServiceImp() {
		
		lista=new ArrayList<>();
		PeliculaServiceImp pelicula=new PeliculaServiceImp();
		SalaCineServiceImp salaCine=new SalaCineServiceImp();
		UsuarioServiceImp  usuario= new UsuarioServiceImp();
		Boleta boleta= new Boleta();
		
		
		boleta.setId(1);
		boleta.setCodigo("1");
		boleta.setPelicula(pelicula.buscarPelicula(1));
		boleta.setSala(salaCine.buscarSalaCine(1));
		boleta.setValor(34343);
		boleta.setAsientosApartados(5);
		boleta.setUsuario(usuario.buscarUsuario(1));
		
		lista.add(boleta);
		
		boleta=new Boleta();
		
		boleta.setId(2);
		boleta.setCodigo("2");
		boleta.setPelicula(pelicula.buscarPelicula(2));
		boleta.setSala(salaCine.buscarSalaCine(2));
		boleta.setValor(34343);
		boleta.setAsientosApartados(5);
		boleta.setUsuario(usuario.buscarUsuario(2));
		
		
		lista.add(boleta);
	}
	
	@Override
	public List<Boleta> listarBoletas() {
		
		return lista;
	}

	@Override
	public void guardarBoleta(Boleta boleta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Boleta buscarBoleta(int id) {
		for (Boleta boleta : lista) {
			if(boleta.getId()==id) {
				return boleta;
			}
		}
		return null;
	}

	@Override
	public void modificarBoleta(Boleta boleta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarBoleta(int id) {
		// TODO Auto-generated method stub
		
	}

	
}
