package edu.ucentral.paginaCineApp.service;

import java.util.List;

import edu.ucentral.paginaCineApp.model.Pelicula;
public interface PeliculaService {

	public List<Pelicula> listarPeliculas();
	public void guardarPelicula(Pelicula pelicula);
	public Pelicula buscarPelicula(int id);
	public void modificarPelicula(Pelicula pelicula);
	public void eliminarPelicula(int id);
}
