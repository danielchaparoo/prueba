package edu.ucentral.paginaCineApp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import edu.ucentral.paginaCineApp.model.SalaCine;

@Service
public class SalaCineServiceImp implements SalaCineService {

	private List<SalaCine> lista;
	
	public SalaCineServiceImp() {
		lista = new ArrayList<>();
		SalaCine salaCine=new SalaCine();
		
		salaCine.setId(1);
		salaCine.setNumeroSala(1);
		salaCine.setCantidadAsientos(45);
		
		lista.add(salaCine);
		
		salaCine=new SalaCine();
		salaCine.setId(2);
		salaCine.setNumeroSala(2);
		salaCine.setCantidadAsientos(45);
		
		lista.add(salaCine);
	
	}
	
	
	@Override
	public List<SalaCine> listarSalasCine() {
	
		return lista;
	}

	@Override
	public void guardarSalaCine(SalaCine salaCine) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SalaCine buscarSalaCine(int id) {
		for (SalaCine salaCine : lista) {
			if(salaCine.getId()==id) {
				return salaCine;
			}
		}
		return null;
	}

	@Override
	public void modificarSalaCine(SalaCine salaCina) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarSalaCine(int id) {
		// TODO Auto-generated method stub
		
	}

	
}
