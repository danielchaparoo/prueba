package edu.ucentral.paginaCineApp.service;
import java.util.List;

import edu.ucentral.paginaCineApp.model.Usuario;

public interface UsuarioService {

	public List<Usuario> listarUsuarios();
	public void guardarUsuario(Usuario usuario);
	public Usuario buscarUsuario(int id);
	public void modificarUsuario(Usuario usuario);
	public void eliminarUsuario(int id);
	
}
