package edu.ucentral.paginaCineApp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import edu.ucentral.paginaCineApp.model.Usuario;

@Service
public class UsuarioServiceImp implements UsuarioService {

	private List<Usuario> lista;
	
	public UsuarioServiceImp() {
		lista= new ArrayList<>();
		Usuario usuario= new Usuario();
		
		usuario.setId(1);
		usuario.setNombre("Usuario 1");
		usuario.setCorreo("ucentral@ucentral.edu.co");
		usuario.setContrasena("Usuario1");
		usuario.setAdministrador(true);	
		
		lista.add(usuario);
		
		usuario= new Usuario();
		usuario.setId(2);
		usuario.setNombre("Usuario 2");
		usuario.setCorreo("ucentral@ucentral.edu.co");
		usuario.setContrasena("Usuario2");
		usuario.setAdministrador(false);
		
		lista.add(usuario);
	
	}

	@Override
	public List<Usuario> listarUsuarios() {
		
		return lista;
	}

	@Override
	public void guardarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario buscarUsuario(int id) {
		for (Usuario usuario : lista) {
			if(usuario.getId()==id) {
				return usuario;
			}
		}
		return null;
	}

	@Override
	public void modificarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarUsuario(int id) {
		// TODO Auto-generated method stub
		
	}
	

}
