package edu.ucentral.paginaCineApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.ucentral.paginaCineApp.model.SalaCine;
import edu.ucentral.paginaCineApp.service.SalaCineService;

@Controller
@RequestMapping(value="/salacines")
public class SalaCineController {
	
	@Autowired
	private SalaCineService service;
	
	
	@GetMapping("/index")
	public String mostrarIndex(Model model) {
		List<SalaCine> lista=service.listarSalasCine();
		model.addAttribute("salaCines",lista);
		return "salacines/index";
	}
	
	@GetMapping("/crear")
	public String crearSalaCine() {
		return "salacines/formSalaCine";
	}
	
	@GetMapping("/ver/{id}")
	public String buscarSalaCine(@PathVariable int id,Model model) {
		SalaCine salaCine= service.buscarSalaCine(id);
		model.addAttribute("salasCine", salaCine);
		return "salacines/formModificar";
	}
	
	@PostMapping("/modificar")
	public String modificarSalaCine(SalaCine salaCine) {
		service.modificarSalaCine(salaCine);
		return "redirect:/salacines/index";
	}
	
	@GetMapping("/eliminar/{id}")
	public String eliminarSalaCine(@PathVariable int id) {
		service.eliminarSalaCine(id);
		return "redirect:/vacantes/index";
	}
	
	@PostMapping("/guardar")
	public String guardarSalaCine(SalaCine salaCine) {
		service.guardarSalaCine(salaCine);
		return "redirect:/salacines/index";
	}
	
	
	
	
	
	
}
