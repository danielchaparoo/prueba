package edu.ucentral.paginaCineApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.ucentral.paginaCineApp.model.Usuario;
import edu.ucentral.paginaCineApp.service.UsuarioService;

@Controller
@RequestMapping(value="/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService service;
	
	@GetMapping("/index")
	public String mostrarIndex(Model model) {
		List<Usuario> lista=service.listarUsuarios();
		model.addAttribute("usuarios",lista);
		
		return "usuarios/index";
	}
	
	@GetMapping("/crear")
	public String crearUsuario(Model model) {
		List<Usuario> lista= service.listarUsuarios();
		model.addAttribute("usuarios",lista);
		return "usuarios/formUsuario";
	}
	
	

    @GetMapping("/ver/{id}")
    public String buscarUsuario(@PathVariable int id,Model model) {
    	Usuario usuario=service.buscarUsuario(id);
    	model.addAttribute(usuario);
    	return "usuarios/formModificar";
    }
    
    
    @PostMapping("/modificar")
    public String modificarUsuario(Usuario usuario) {
    	service.modificarUsuario(usuario);
    	return "redirect:/usuarios/index";
    }
    
    @GetMapping("/eliminar/{id}")
    public String eliminarUsuario(@PathVariable int id) {
    	service.eliminarUsuario(id);
    	return "redirect:/usuarios/index";
    }
    
    @PostMapping("/guardar")
    public String guardarVacante(Usuario usuario) {
    	service.guardarUsuario(usuario);
    	return "redirect:/usuarios/index";
    }
    
}
