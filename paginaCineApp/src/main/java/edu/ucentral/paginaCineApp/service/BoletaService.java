package edu.ucentral.paginaCineApp.service;

import java.util.List;

import edu.ucentral.paginaCineApp.model.Boleta;

public interface BoletaService {

	public List<Boleta> listarBoletas();
	public void guardarBoleta(Boleta boleta);
	public Boleta buscarBoleta(int id);
	public void modificarBoleta(Boleta boleta);
	public void eliminarBoleta(int id);
}
