package edu.ucentral.paginaCineApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.ucentral.paginaCineApp.model.Horario;
import edu.ucentral.paginaCineApp.model.Pelicula;
import edu.ucentral.paginaCineApp.model.SalaCine;
import edu.ucentral.paginaCineApp.service.HorarioService;
import edu.ucentral.paginaCineApp.service.PeliculaService;
import edu.ucentral.paginaCineApp.service.SalaCineService;

@Controller
@RequestMapping(value="/peliculas")
public class PeliculaController {

	@Autowired
	private PeliculaService service;
	
	@Autowired
	private SalaCineService service1;
	
	@Autowired
	private HorarioService service2;
	
	@GetMapping("/index")
	public String mostrarIndex(Model model) {
		List<Pelicula> lista=service.listarPeliculas();
		model.addAttribute("peliculas",lista);
		return "peliculas/index";
	}
	
	@GetMapping("/crear")
	public String crearPelicula(Model model) {
		List<SalaCine> lista=service1.listarSalasCine();
		List<Horario>  lista2=service2.listarHorarios();
		
		model.addAttribute("salasCine",lista);
		model.addAttribute("horarios",lista2);
		
		return "peliculas/formPelicula";
	}
	
	@GetMapping("/ver/{id}")
	public String buscarPelicula(@PathVariable int id,Model model) {
		Pelicula pelicula=service.buscarPelicula(id);
		model.addAttribute("peliculas", pelicula);
		return "peliculas/formModificar";
	}
	
	@PostMapping("/modificar")
	public String modificarPelicula(Pelicula pelicula) {
		service.modificarPelicula(pelicula);
		return "redirect:/peliculas/index";
	}
	
	@GetMapping("/eliminar/{id}")
	public String eliminarPelicula(@PathVariable int id) {
		service.eliminarPelicula(id);
		return "redirect:/peliculas/index";
	}
	
	@PostMapping("/guardar")
	public String  guardarVacante(Pelicula pelicula) {
		service.guardarPelicula(pelicula);
		return "redirect:/pelicula/index";
	}
	
}