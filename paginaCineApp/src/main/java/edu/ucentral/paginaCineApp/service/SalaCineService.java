package edu.ucentral.paginaCineApp.service;

import java.util.List;

import edu.ucentral.paginaCineApp.model.SalaCine;

public interface SalaCineService {

	public List<SalaCine> listarSalasCine();
	public void guardarSalaCine(SalaCine salaCine);
	public SalaCine buscarSalaCine(int id);
	public void modificarSalaCine(SalaCine salaCina);
	public void eliminarSalaCine(int id);
	
}
