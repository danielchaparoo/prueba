package edu.ucentral.paginaCineApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaginaCineAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaginaCineAppApplication.class, args);
	}

}
